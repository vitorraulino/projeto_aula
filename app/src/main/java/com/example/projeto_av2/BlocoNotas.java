package com.example.projeto_av2;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class BlocoNotas extends AppCompatActivity {

    SQLiteDatabase bd;
    EditText txt_inputL;
    String conteudo;
    String nome;
    int existe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bloco_notas);

        txt_inputL = findViewById(R.id.txt_inputL);

        Intent i = getIntent();
        Bundle b = i.getExtras();
        existe = Integer.parseInt(b.getString("ex"));
        nome = b.getString("nome");

        bd = openOrCreateDatabase("blocos", MODE_PRIVATE, null);
        bd.execSQL("CREATE TABLE IF NOT EXISTS bloco(id INTEGER PRIMARY KEY AUTOINCREMENT,nome VARCHAR,conteudo TEXT)");

        //if(existe == 1){
        //    Cursor cursor = bd.rawQuery("SELECT conteudo  FROM  bloco WHERE nome='" + nome + "'",null);

        //}

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Salvar", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();

                conteudo = txt_inputL.toString();

                if (existe == 1) {
                    ContentValues cv = new ContentValues();
                    cv.put("conteudo", conteudo);
                    bd.update("bloco", cv, "nome=" + nome, null);
                } else {
                    AlertDialog.Builder telaPesquisa = new AlertDialog.Builder(getApplicationContext());
                    telaPesquisa.setTitle("Salvar");
                    telaPesquisa.setMessage("Informe o nome");
                    telaPesquisa.setNegativeButton("Cancelar", null);
                    DialogInterface.OnClickListener o = new DialogInterface.OnClickListener() {
                        @Override
                        public void
                        onClick(DialogInterface dialog,int which) {
                            ContentValues cv = new ContentValues();
                        }};
                        telaPesquisa.setPositiveButton("Salvar", o);;
                    };
                }
            });
        }
    }

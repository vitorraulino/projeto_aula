package com.example.projeto_av2;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.content.Intent;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    ListView lv;
    SQLiteDatabase bd;
    ArrayList <String> arrStr = new ArrayList<String>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lv = findViewById(R.id.lv);

        bd = openOrCreateDatabase("blocos",MODE_PRIVATE,null);
        bd.execSQL("CREATE TABLE IF NOT EXISTS bloco(id INTEGER PRIMARY KEY AUTOINCREMENT,nome VARCHAR,conteudo TEXT)");

        //bd.execSQL("INSERT INTO bloco (nome,conteudo) values ('Teste','TESTEEEEEEFKLAJFLKJLKJ')");

        Cursor cursor = bd.rawQuery("SELECT id, nome, conteudo  FROM  bloco ",null);

        if(cursor.moveToNext()) {
            cursor.moveToFirst();
            do {
                String strAux = cursor.getString(cursor.getColumnIndex("nome"));
                arrStr.add(strAux);
            } while (cursor.moveToNext());
            ArrayAdapter<String> adpt = new ArrayAdapter<String>(
                    this,
                    android.R.layout.simple_list_item_1,
                    arrStr
            );

            lv.setAdapter(adpt);

            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    //Seu codigo aqui
                    Intent intent = new Intent(getApplicationContext(), BlocoNotas.class);
                    intent.putExtra("ex",1 + "");
                    intent.putExtra("nome",(String)parent.getItemAtPosition(position));
                    startActivity(intent);
                }
            });
        }
    }
    public void excluiTudo(View v){
        bd.execSQL("DELETE FROM bloco");
    }
    public void adicionarNovo(View v){
        Intent intent = new Intent(this, BlocoNotas.class);

        intent.putExtra("ex",0 + "");
        intent.putExtra("nome","");
        startActivity(intent);
    }
}
